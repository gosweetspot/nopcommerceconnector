# README #

## What is this repository for? ##

This plugin provides a REST interface for order/customer information for nopCommerce.

## How do I get set up? ##

### Building from source ###

Clone the source and place in the Plugins folder. Build the project. Check to see if the plugin has appeared in the Nop.Web\Plugins folder. You may need to adjust the build output destination.

### Ready built version ###

Play the plugin in the Nop.Web\Plugins folder.

### Setup ###

You may also need to change the Version variable in the Description.txt to match your version of nopCommerce.

Install the plugin on the admin panel, configure it and generate a hash key.

Test the plugin by running the following command in chrome tools (replace the apiToken with your own):

	$.post('/Api/GetOrdersByStatus?shippingStatuses=10&shippingStatuses=20&purchaseStatuses=10,20,30,35,40,50&apiToken=YOUR-TOKEN-HERE',	
	    function (response) { console.log(response); });

You should receive some order data as a response.

Set up the connection on GoSweetSpot at: https://ship.gosweetspot.com/integration/new?source=NopCommerce

Give the integration a few moments to synchronize and your orders will appear on GoSweetSpot.

### Who do I talk to? ###

Please contact the dev team at GoSweetSpot - support@gosweetspot.com